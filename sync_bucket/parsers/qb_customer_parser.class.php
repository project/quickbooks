<?php
/**
 * Parsers a given customer.xml file
 * and either updates a customers information
 * or the customer gets inserted into the database.
 * 
 */
class qb_customer_parser {
    private $xml_parser = NULL;
    private $processed_customers = 0;
    private $errors = 0;
    private $element_stack = array();
    private $parent_element = "";
    private $current_element = "";
    private $sql = "";
    private $customer_params = array();
    
    
    function get_error_count() {
      return $this->errors;
    }
    
    function get_processed_count() {
      return $this->processed_customers;
    }
    
    function qb_customer_parser() {
      # Register the XML parser functions that
      # we are going to use.
      //TODO: There is a descrepency in 4.3->5.0, and certain 5.0
      // versions dealing with registering the default character data function
      // vs the default element handler. Deal with this.
      
      $this->xml_parser = xml_parser_create();
      xml_set_object($this->xml_parser, $this);
      #$this->processed_customers = variable_get('quickbooks_cron_processed_customers', 0);
      
      # Main XML Root containing errors
      xml_set_element_handler($this->xml_parser, 'customer_element_open', 'customer_element_close' );
      # CData Handler 
      xml_set_character_data_handler($this->xml_parser, 'customer_c_data_handler');
      # Default handler for any junk or items we are not reading
      xml_set_default_handler($this->xml_parser, 'default_element_handler');
      
      
      
      
    }
    
    function default_element_handler($parser, $element = "", $attributes = array()) {
      return;
    }
    
    function parse($file_path) {
      xml_parse($this->xml_parser, file_get_contents($file_path));
      return $this->processed_customers;
    }
    
    function commit_customer() {
      
      $sql = "";
      $sql = "INSERT INTO {quickbooks_customers}(" .
          "cid, list_id, created, modified, first_name, last_name," .
          "contact_phone, contact_email, contact_name, is_active, company_name," .
          "bill_addr_1, bill_addr_2, bill_city, bill_state, bill_zip, " .
          "ship_addr_1, ship_addr_2, ship_city, ship_state, ship_zip, " .
          "sub_level, parent_list_id, parent_cid, job_status" .
          " ) VALUES (" .
          "%d, '%s', %d, %d, '%s', '%s'," .
          "'%s', '%s', '%s', %d, '%s'," .
          "'%s', '%s', '%s', '%s', '%s'," .
          "'%s', '%s', '%s', '%s', '%s'," .
          "%d, '%s', '%d', '%s')";
     $params = $this->customer_params;
     $this->customer_params = array();
     
     # Write all the values. This will make it easier than trying to match array sequences up since
     # we may be dealing with diff. qbXML versions in the future.
     db_query('START TRANSACTION'); // TODO DB Should handle start of trans with a db_start_trans call
     $ccid = db_result(db_query("SELECT cid FROM {quickbooks_customers} WHERE list_id = '%s'", trim($params['list_id'])));
     if(is_numeric($ccid)) {
       $new_cid = $ccid;
       db_query('DELETE FROM {quickbooks_customers} WHERE cid = %d', $new_cid);
     }
     else {
      $new_cid = db_next_id("{quickbooks_customers}_cid");
     }
     $try = db_query($sql, $new_cid, $params['list_id'], $params['created'], $params['modified'],
      $params['first_name'], $params['last_name'], $params['contact_phone'], $params['contact_email'], $params['contact_name'],
      $params['is_active'], $params['company_name'], $params['bill_addr_1'], $params['bill_addr_2'], $params['bill_city'], $params['bill_state'],
      $params['bill_zip'], $params['ship_addr_1'], $params['ship_addr_2'], $params['ship_city'], $params['ship_state'], $params['ship_zip'], 
      $params['sub_level'], $params['parent_list_id'], $params['parent_cid'], $params['job_status']);
      
      $this->processed_customers++;
      if(!$try) {
        $this->errors++;
        $rollback = TRUE;
      }
      // Check rollback/commit phase
      if($try === FALSE) {
        db_query('COMMIT');
      }
      else {
        db_query('ROLLBACK');
      }
      
      return $try;
    }
    
    function customer_c_data_handler($parser, $cdata) {
      
      # Build the current customers array
      if($this->closing_tag) {
        return;
      }
      switch($this->current_element) {
        # General Customer Inforamtion
        // TODO: Bring in all 'lists', and use the list_id to reference items
        // such as the customers terms (net 10, etc.) etc.
        case 'LISTID':
          if($this->parent_element == 'CUSTOMERRET') {
            $this->customer_params['list_id'] = $cdata;
          }
          else if($this->parent_element == 'PARENTREF') {
            $this->customer_params['parent_list_id'] = trim($cdata);
            $cid = db_result(db_query("SELECT cid FROM {quickbooks_customers} WHERE list_id = '%s'", $cdata));
            if(is_numeric($cid)) {
              $this->customer_params['parent_cid'] = $cid;
            }
            else {
              $this->customer_params['parent_cid'] = NULL;
            }
          }
          break;
        case 'COMPANYNAME':
          $this->customer_params['company_name'] = $cdata;
          break;
        case 'SUBLEVEL':
          $this->customer_params['sub_level'] = $cdata;
          break;
        case 'JOBSTATUS':
          $this->customer_params['job_status'] = $cdata;
          break;
        case 'TIMECREATED':
          $this->customer_params['created'] = strtotime($cdata);
          break;
        case 'TIMEMODIFIED':
          $this->customer_params['modified'] = strtotime($cdata);
          break;
        case 'FIRSTNAME':
          $this->customer_params['first_name'] = $cdata;
          break;
        case 'LASTNAME':
          $this->customer_params['last_name'] = $cdata;
          break;
        case 'PHONE':
          $this->customer_params['contact_phone'] = $cdata;
          break;
        case 'EMAIL':
          $this->customer_params['contact_email'] = $cdata;
          break;
        case 'CONTACT':
          $this->customer_params['contact_name'] = $cdata;
          break;
        case 'ISACTIVE':
          $this->customer_params['is_active'] = $cdata == "true";
          break;
        # End Customer General Information
        # Addresses Section
        case 'ADDR1':
          if($this->parent_element == "BILLADDRESS") {
            $this->customer_params['bill_addr_1'] = $cdata;
          }
          if($this->parent_element == "SHIPADDRESS") {
            $this->customer_params['ship_addr_1'] = $cdata;
          }
        case 'ADDR2':
          if($this->parent_element == "BILLADDRESS") {
            $this->customer_params['bill_addr_2'] = $cdata;
          }
          if($this->parent_element == "SHIPADDRESS") {
            $this->customer_params['ship_addr_2'] = $cdata;
          }
          break;
        case 'CITY':
          if($this->parent_element == "BILLADDRESS") {
            $this->customer_params['bill_city'] = $cdata;
          }
          if($this->parent_element == "SHIPADDRESS") {
            $this->customer_params['ship_city'] = $cdata;
          }
          break;
        case 'STATE':
          if($this->parent_element == "BILLADDRESS") {
            $this->customer_params['bill_state'] = $cdata;
          }
          if($this->parent_element == "SHIPADDRESS") {
            $this->customer_params['ship_state'] = $cdata;
          }
          break;
        case 'POSTALCODE':
          if($this->parent_element == "BILLADDRESS") {
            $this->customer_params['bill_zip'] = $cdata;
          }
          if($this->parent_element == "SHIPADDRESS") {
            $this->customer_params['ship_zip'] = $cdata;
          }
          break;
        # End Addresses Section
      }
      
      return;
    }
    
    function customer_element_open($parser, $element, $attributes) {
      
      $this->current_element = $element;
      $this->parent_element = $this->array_peek($this->element_stack);
      array_push($this->element_stack, $element);
      
      $this->closing_tag = FALSE;
      $this->open_tag = TRUE;
      return;
    }
    
    function customer_element_close($parser, $element) {
      $this->current_element = array_pop($this->element_stack);
      $this->parent_element = $this->array_peek($this->element_stack);
      if($element == 'CUSTOMERRET' ) {
        $this->commit_customer();
        
      }
      $this->closing_tag = TRUE;
      $this->open_tag = FALSE;
      
      return;
    }
    
    /**
     * My custom "peek" function
     * The stack is passed by value, not 
     * by Reference/address.
     * 
     */
    function array_peek($stack) {
      $peek = array_pop($stack);
      array_push($stack, $peek);
      return $peek;
    }
    
    
}
?>