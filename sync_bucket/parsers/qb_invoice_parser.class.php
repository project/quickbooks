<?php

 
 /**
 * Parsers a given invoice.xml file
 * and either updates a customers information
 * or the customer gets inserted into the database.
 * 
 */
 
class qb_invoice_parser {
    
    private $xml_parser = NULL;
    private $processed_invoices = 0;
    private $errors = 0;
    private $element_stack = array();
    private $parent_element = "";
    private $current_element = "";
    private $sql = "";
    private $invoice_params = array();
    private $in_group = FALSE;
    private $invoice_d_group_header = array();
    
    function get_error_count() {
      return $this->errors;
    }
    
    function get_processed_count() {
      return $this->processed_invoices;
    }
    
    function qb_invoice_parser() {
      # Register the XML parser functions that
      # we are going to use.
      //TODO: There is a descrepency in 4.3->5.0, and certain 5.0
      // versions dealing with registering the default character data function
      // vs the default element handler. Deal with this.
      
      $this->xml_parser = xml_parser_create();
      xml_set_object($this->xml_parser, $this);
      #$this->processed_invoices = variable_get('quickbooks_cron_processed_invoices', 0);
      
      # Main XML Root containing errors
      xml_set_element_handler($this->xml_parser, 'invoice_element_open', 'invoice_element_close' );
      # CData Handler 
      xml_set_character_data_handler($this->xml_parser, 'invoice_c_data_handler');
      # Default handler for any junk or items we are not reading
      xml_set_default_handler($this->xml_parser, 'default_element_handler');
      
      # Other intializing 
      $this->invoice_d_hold = array();
      $this->invoice_d = array();
      $this->invoice_params = array();
      $this->invoice_d_group_header = array();
      $this->in_group = FALSE;
      
      
      
    }
    
    function default_element_handler($parser, $element = "", $attributes = array()) {
      return;
    }
    
    function parse($file_path) {
      xml_parse($this->xml_parser, file_get_contents($file_path));
      return $this->processed_invoices;
    }
    
    /**
     * TODO Perhaps if no customer is found, you should not import this invoice,
     * and flag an error some where?
     * 
     * Takes a ListID and looks up customer
     * in the local database and returns either 
     * the customers cid in the local system,
     * or false.
     * 
     * @param $list_id The Quickbooks ListID for this customer.
     * @return $cid The customer's cid in the local database.
     * 
     */
    private function lookup_customer($list_id) {
      $cid = db_result(db_query("SELECT cid FROM {quickbooks_customers} WHERE LTRIM(RTRIM(list_id)) = '%s'", TRIM($list_id)));
      return $cid;
    } 
    
    function commit_invoice() {
      /**
       * This is setup up so that on the first error
       * it stops processing htis invoice and 
       * rolls back the transaction. 
       * You'll thank me for that when you run your 
       * first large file.
       */
       
      $sql = "";
      $sql_args = array();
      
      /**
       * If this invoice already exists, we need to update the invoice.
       * Since the application logic to see if there are diff line items, etc.
       * pretty heft, we should just use a transaction, remove the invoice 
       * and re-insert the invoice. Thie invoice should be re-inserted 
       * using the original invoice_id so that other modules, functions, 
       * and mappings can still have a ref. to the invoice. 
       */
      $update_invoice_id = db_result(db_query('SELECT invoice_id FROM {quickbooks_invoice_h} WHERE ref_num = %d', trim($this->invoice_params['ref_num']))) > 0;
      
      $rollback = FALSE;
      db_query('START TRANSACTION');
      # First do the invoice header
      $invoice_id = is_numeric($update_invoice_id) ? $update_invoice_id : db_next_id("{quickbooks_invoice_h}_invoice_id");
      $sql = "INSERT INTO {quickbooks_invoice_h}(" .
          "invoice_id, ref_num, cid, txn_id, created, modified, txn_date, is_pending, is_finance_charge, terms, due_date, " .
          "ship_date, sub_total, tax_rate, tax_total, balance, applied_amt) " .
          "VALUES(%d, '%s', %d, '%s', %d, %d, %d, '%s', '%s', '%s', %d, %d, '%f', '%f', '%f', '%f', '%f')";
      $sql_args[] = $invoice_id;
      $sql_args[] = $this->invoice_params['ref_num'];
      $sql_args[] = $this->invoice_params['cid'];
      $sql_args[] = $this->invoice_params['txn_id'];
      $sql_args[] = $this->invoice_params['created'];
      $sql_args[] = $this->invoice_params['modified'];
      $sql_args[] = $this->invoice_params['txn_date'];
      $sql_args[] = $this->invoice_params['is_pending'];
      $sql_args[] = $this->invoice_params['is_finance_charge'];
      $sql_args[] = $this->invoice_params['terms'];
      $sql_args[] = $this->invoice_params['due_date'];
      $sql_args[] = $this->invoice_params['ship_date'];
      $sql_args[] = $this->invoice_params['sub_total'];
      $sql_args[] = $this->invoice_params['tax_rate'];
      $sql_args[] = $this->invoice_params['tax_total'];
      $sql_args[] = $this->invoice_params['balance'];
      $sql_args[] = $this->invoice_params['applied_amt'];

      $try = db_query($sql, $sql_args);
      if(!$try) {
        $this->errors++;
        $rollback = TRUE;
      }
      else {
        # Now, loop through
        // TODO Check for redundant trims.
        foreach($this->invoice_d as $k => $invoice_d) {
          //echo "<pre>" . print_r($invoice_d, 1) . "</pre>";
          /**
           * For now, we will ignore class line items that will not
           * have an associated list_id. Perhaps later we'll add this 
           * if someone needs this functinality.
           * But, we don't want to ignore line item descriptions
           * 
           */
          /*if(trim($invoice_d['item_list_id']) == '' AND FALSE) {
            continue;
          }*/
          $line_id = db_result(db_query("SELECT line_id FROM {quickbooks_invoice_d} WHERE item_list_id = '%s' AND invoice_id = %d", trim($invoice_d['item_list_id']), $invoice_id));
          if(is_numeric($line_id)) {
            db_query("DELETE FROM {quickbooks_invoice_d} WHERE line_id = %d", $line_id);
          }
          else {
            $line_id = db_next_id("{quickbooks_invoice_d}_line_id");  
          }
  
          $sql = "INSERT INTO {quickbooks_invoice_d}(line_id, invoice_id, " .
              "item_list_id, item_name, item_desc, item_class_name, item_qty, item_rate, item_line_total, item_group_list_id, item_group_line, item_service_date) " .
              "VALUES(%d, %d, '%s', '%s', '%s', '%s', %d, %f, %f, '%s', %d, '%d')";
          $sql_args = array();
          $sql_args[] = $line_id;
          $sql_args[] = $invoice_id;
          $sql_args[] = $invoice_d['item_list_id'];
          $sql_args[] = trim($invoice_d['item_name']);
          $sql_args[] = trim($invoice_d['item_desc']);
          $sql_args[] = $invoice_d['item_class_name'];
          $sql_args[] = $invoice_d['item_qty'] == '' ? 1 : $invoice_d['item_qty'];
          $sql_args[] = $invoice_d['item_rate'];
          $sql_args[] = $invoice_d['item_line_total'];
          $sql_args[] = $invoice_d['group_list_id'];
          $sql_args[] = $invoice_d['group_line']; //is_numeric($invoice_d['group_line_header']) ? $invoice_d['group_line_header'] : 0; // For debugging purposes
          $sql_args[] = $invoice_d['item_service_date'] ;
          $try = db_query($sql, $sql_args);
          //echo "$sql <br/> <pre>" . print_r($sql_args, 1) . "</pre><br />"; // For debugging
          if(!$try) {
            $this->errors++;
            $rollback = TRUE;
            break;
          }
        }
      }
      
      // TODO Transactions should be started in the db layer with db_start_trans(); Currently drupal doesn't handle/do this though
      if($rollback === FALSE) {
        db_query('COMMIT');
      }
      else {
        db_query('ROLLBACK');
      }
      $this->invoice_d = array();
      $this->processed_invoices++;

      return $try;
    }
    
    function invoice_c_data_handler($parser, $cdata) {

      # Build the current invoicess array
      if($this->closing_tag) {
        return;
      }
      switch($this->current_element) {
        # General Customer Inforamtion
        // TODO: Bring in all 'lists', and use the list_id to reference items
        // such as the invoices terms (net 10, etc.) etc.
        
        # This is the start of an invoice
        case 'INVOICERET':
          # Reset the invoice params on a new open tag
          $this->invoice_params = array();
          break;
        # The transaction ID
        case 'TXNID':
          $this->invoice_params['txn_id'] = $cdata;
          break;
        case 'TIMECREATED':
          $this->invoice_params['created'] = strtotime($cdata);
          break;
        case 'TIMEMODIFIED':
          $this->invoice_params['modified'] = strtotime($cdata);
          break;
        # Service Date
        case 'SERVICEDATE':
          $this->invoice_d_hold['item_service_date'] = strtotime($cdata);
          break;
        # Applied Amt
        case 'APPLIEDAMOUNT':
          $this->invoice_params['applied_amt'] = trim($cdata);
          break;
        # Invoice date
        case 'TXNDATE':
          $this->invoice_params['txn_date'] = strtotime($cdata);
          break;
        case 'ISPENDING':
          $this->invoice_params['is_pending'] = trim($cdata);
          break;
        # Lets us know if this invoice is just a finanace charge
        case 'ISFINANCECHARGE':
          $this->invoice_params['is_finance_charge'] = trim($cdata);
          break;
        # For now, we just want the FullName feature of TermsRef
        case 'FULLNAME':
          if($this->parent_element == 'TERMSREF') {
            $this->invoice_params['terms'] = $cdata;
          }
          else if($this->parent_element == 'ITEMREF' || $this->parent_element == 'ITEMGROUPREF') {
            if($this->parent_element == 'ITEMGROUPREF') {
              $this->invoice_d_group_header['item_name'] = $cdata;
            }
            else {
              $this->invoice_d_hold['item_name'] = $cdata;
            }
          }
          else if($this->parent_element == 'CLASSREF' && $this->in_invoice_line_item) {
            $this->invoice_d_hold['item_class_name'] = $cdata;
          }
          break;
        case 'LISTID':
          if($this->parent_element == 'ITEMREF' || $this->parent_element == 'ITEMGROUPREF') {
              if($this->parent_element == 'ITEMGROUPREF') {
                $this->current_group_list_id = $cdata;
                $this->invoice_d_group_header['item_list_id'] = $cdata;
              }
              else {
                $this->invoice_d_hold['item_list_id'] = $cdata;
              }
          }
          else if($this->parent_element == 'CUSTOMERREF') {
            $this->invoice_params['cid'] = db_result(db_query("SELECT cid FROM {quickbooks_customers} WHERE TRIM(list_id) = '%s'", trim($cdata)));
          }
          break;
        case 'DUEDATE':
          $this->invoice_params['due_date'] = strtotime(trim($cdata));
          break;
        case 'TOTALAMOUNT':
          $this->invoice_d_group_header['item_line_total'] = $cdata;
          break;
        case 'SHIPDATE':
          $this->invoice_params['ship_date'] = strtotime($cdata);
          break;
        case 'SUBTOTAL':
          $this->invoice_params['sub_total'] =$cdata;
          break;
        case 'SALESTAXPERCENTAGE':
          $this->invoice_params['tax_rate'] = $cdata;
          break;
        case 'SALESTAXTOTAL':
          $this->invoice_params['tax_total'] = $cdata;
          break;
        case 'BALANCEREMAINING':
          $this->invoice_params['balance'] = $cdata;
          break;
        # Mostly invoice_d stuff below
        case 'DESC':
          if($this->parent_element == 'INVOICELINEGROUPRET') {
            // Need to append becuase sometimes the descriptoin has 
            // line breaks in which this will be called more than once.
            $this->invoice_d_group_header['item_desc'] .= $cdata;
          }
          else {
            // Need to append becuase sometimes the descriptoin has 
            // line breaks in which this will be called more than once.
            $this->invoice_d_hold['item_desc'] .= $cdata;
          }
          break;
        case 'QUANTITY':
          if(is_numeric($cdata)) {
            $this->invoice_d_hold['item_qty'] = $cdata;
          }
          else {
            $this->invoice_d_hold['item_qty'] = 1;
          }
          break;
        case 'RATE':
          $this->invoice_d_hold['item_rate'] = $cdata;
          break;
        case 'AMOUNT':
          $this->invoice_d_hold['item_line_total'] = $cdata;
          break; 
        case 'REFNUMBER':
          $this->invoice_params['ref_num'] = $cdata;
          break;
      }
      
      return;
    }
    
    function invoice_element_open($parser, $element, $attributes) {
      
      $this->current_element = $element;
      $this->parent_element = $this->array_peek($this->element_stack);
      array_push($this->element_stack, $element);
      
      $this->closing_tag = FALSE;
      $this->open_tag = TRUE;
      
      if($element == 'INVOICELINERET') {
        $this->in_invoice_line_item = TRUE;
      }
      else if($element == 'INVOICELINEGROUPRET') {
        $this->in_group = TRUE;
      }
      return;
    }
    
    /**
     * @author Earnest Berry III <eberry@washsq.com>
     * 
     * @param object $parser the parsing object
     * @param string $element the closing tag as a string
     * @return void
     */
    function invoice_element_close($parser, $element) {
      $this->current_element = array_pop($this->element_stack);
      $this->parent_element = $this->array_peek($this->element_stack);
      
      switch ($element) {
        case 'INVOICERET':
          $this->commit_invoice();
          break;
        case 'ITEMGROUPREF':
          break;
        case 'INVOICELINERET':
        case 'INVOICELINEGROUPRET':
          /**
           * If we're in a group, we need to add 
           * the group items to the appropriate array
           * The group_line tells us that the item is part of a group
           */
          if($element == 'INVOICELINERET') {
            if($this->in_group) {
              $this->invoice_d_hold['group_line'] = 1;
              /**
               * This if is here seperatley so that we can 
               * tell the parser what group we're currently 
               * dealing with by setting the groups list id
               * to a variable.
               * 
               * Prehaps I should look into making this array 
               * to deal with kits inside of gets so that the 
               * parser will support any structure with 
               * N kits on L (line item)
               * 
               * Removed this and I am just setting a simple group_ref element to be added to the 
               * table.
               */
              $this->invoice_d_hold['group_list_id'] = $this->current_group_list_id;
            }
            else {
              $this->invoice_d_hold['group_line'] = 0;
            }
          }
          
          # Take care of group vs. line
          if($element == 'INVOICELINEGROUPRET') {
            $this->invoice_d[] = $this->invoice_d_group_header;
            $this->invoice_d_group_header = array();
            $this->in_group = FALSE;
            $this->current_group_list_id = NULL;
          }
          else {
            //echo "<pre>" . print_r($this->invoice_d_hold, 1) . "</pre>";
            $this->invoice_d[] = $this->invoice_d_hold;  
          }
          
          $this->invoice_d_hold = array();
          $this->in_invoice_line_item = FALSE;
          
          
          break;
      }
      $this->closing_tag = TRUE;
      $this->open_tag = FALSE;
      
      return;
    }
    
    /**
     * My custom "peek" function
     * The stack is passed by value, not 
     * by Reference/address.
     * TODO: So if it's passed by value, 
     * we don't need to push it back on right?
     * 
     */
    function array_peek($stack) {
      $peek = array_pop($stack);
      array_push($stack, $peek);
      return $peek;
    }
    
}
?>